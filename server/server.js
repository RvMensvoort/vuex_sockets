
const express = require('express');
const app = express();

const server = app.listen(3001, function() {
    console.log('server running on port 3001');
});

const io = require('socket.io')(server);

var connectedColours =  [];

io.on('connection', (socket) => {
	var availableColours = [ 'red', 'green', 'blue', 'purple' ];

	/* Create and join a new room */
	socket.on('CREATE_NEW_ROOM', (roomId) => {
		console.log('room created: ' + roomId);
		io.of('/' . roomId);
		socket.join(roomId);
		connectedColours.push([roomId]);
	});

	socket.on('ADD_USER', (roomId) => {
		let colours = availableColours;

		// sort avaiable colours from connected colours
		for (var roomIdKey in connectedColours) {
			let roomColours = connectedColours[roomIdKey]
			if(roomColours[0] == roomId) {
				// remove already used colours 
				for (var colourKey in roomColours) {
					if(colours.includes(roomColours[colourKey])) {
						colours.splice(colours.indexOf(roomColours[colourKey]), 1);
					}
				}
			}
		}

		// select a random colour
		let randomColour = colours[Math.floor(Math.random() * colours.length)];

		// add selectd colour to connected colour array
		for (var key in connectedColours) {
			if(connectedColours[key][0] == roomId) {
				connectedColours[key].push(randomColour);
			}
		}

		// send chosen colour to joined user
		socket.emit('RETURN_COLOUR', randomColour);
		socket.colour = randomColour;
		socket.roomId = roomId;

		// echo globally (all clients) that a person has connected
		io.emit('USER_JOINED', {
			roomId: socket.roomId, 
			colour: socket.colour 
		});
	});

	socket.on('disconnect', () => {
		if(socket.roomId !== undefined) {
			console.log('disconnected', socket.colour);

			io.emit('USER_LEFT', {
				roomId: socket.roomId, 
				colour: socket.colour 
			});
		}
	});

	socket.on('GET_AVAILABLE_ROOMS', () => {
		socket.emit('AVAILABLE_ROOMS', {
			rooms: io.sockets.adapter.rooms,
			connectedColours: connectedColours
		});
	});	
	socket.on('MESSAGE', (data) => {
		io.emit('MESSAGE_TO_ALL', data);
	});	
});

