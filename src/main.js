import Vue from 'vue'
import store from './store/store'
import App from './App.vue'
// import * as io from 'socket.io-client'
// import VueSocketIO from 'vue-socket.io'
import { routes } from './router'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

Vue.config.productionTip = false

const router = new VueRouter({
  routes,
  mode: 'history'
});

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')