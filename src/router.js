import Mobile from './components/mobile.vue';
import Home from './components/home.vue';
import One from './components/one.vue';
import Two from './components/two.vue';

export const routes = [
	{ path: '/home', component: Home },
	{ path: '/mobile', component: Mobile },
	{ path: '/one', component: One },
	{ path: '/two', component: Two },
];