import Vue from 'vue'
import Vuex from 'vuex'
import io from 'socket.io-client';

Vue.use(Vuex);

export default new Vuex.Store({
	state: {
    socket: io('localhost:3001'),
    message: '1'
  },
  getters: {
    message: state => {
      console.log(message);
      return state.message;
    }
  },
  mutations: {
    updateMessage(state, payload) {
      console.log(payload);
      state.message = payload;
    }
  }
})